# OpenML dataset: california_housing

https://www.openml.org/d/44929

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Information on the variables was collected using all the block groups in California from the 1990 Census. In this sample a block group on average includes 1425.5 individuals living in a geographically compact area. Naturally, the geographical area included varies inversely with the population density. Distances among the centroids of each block group were computed as measured in latitude and longitude. All the block groups reporting zero entries for the independent and dependent variables were excluded. The final data contained 20,640 observations on 9 variables. 

Each row in the dataset represents one census block group. A block group is the smallest geographical unit for which the U.S. Census Bureau publishes sample data (a block group typically has a population of 600 to 3,000 people).

The goal of the dataset is to predict the median house value. The original dataset description advised to predict the value using logarithmic transform.


**Attribute Description**

Census block group describing features:

1. *longitude* 
2. *latitude*
3. *housingMedianAge*
4. *totalRooms*
5. *totalBedrooms*
6. *population*
7. *households*
8. *medianIncome*
9. *medianHouseValue* - target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44929) of an [OpenML dataset](https://www.openml.org/d/44929). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44929/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44929/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44929/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

